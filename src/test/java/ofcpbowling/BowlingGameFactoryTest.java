package ofcpbowling;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class BowlingGameFactoryTest {

    private BowlingGame game;

    @Before
    public void beforeEachTest() {
        game = new BowlingGameFactory().create();
    }
    
    @Test
    public void gutterGame() {
        rollScores(new int[] { 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0 });
        checkScoreIs(0);
    }
    
    @Test
    public void allOnes() {
        rollScores(new int[] { 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1 });
        checkScoreIs(20);
    }
    
    @Test
    public void singleSpare() {
        rollScores(new int[] { 0,0, 4,6, 3,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0 });
        checkScoreIs(16);
    }
    
    @Test
    public void spareInTheFinalFrame() {
        rollScores(new int[] { 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 3,7,2 });
        checkScoreIs(12);
    }
    
    @Test
    public void strike() {
        rollScores(new int[] { 0,0, 10, 3,2, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0 });
        checkScoreIs(20);
    }
    
    @Test
    public void strikeInTheFinalFrame() {
        rollScores(new int[] { 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 10,2,8 });
        checkScoreIs(20);
    }
    
    @Test
    public void perfectGame() {
        rollScores(new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,10,10 });
        checkScoreIs(300);
    }
    
    private void rollScores(int[] scores) {
        for (int i = 0; i < scores.length; i++) {
            game.roll(scores[i]);
        }
    }
    
    private void checkScoreIs(int score) {
        assertThat(game.score(), is(score));
    }
}
