package ofcpbowling;


public interface FrameState {

    void roll(int score, Frame frame);

}
