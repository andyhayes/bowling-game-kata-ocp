package ofcpbowling;

public class StandardFrame implements Frame {
    
    private final Frame nextFrame;
    private FrameState state;
    
    private int frameScore;
    
    public StandardFrame(Frame nextFrame, FrameState initialState) {
        this.nextFrame = nextFrame;
        this.state = initialState;
    }

    public void roll(int score) {
        state.roll(score, this);
    }

    public void passScoreToNextFrame(int score) {
        this.nextFrame.roll(score);
    }

    public void addScoreToFrame(int score) {
        this.frameScore += score;
    }

    public int totalScore() {
        return frameScore + nextFrame.totalScore();
    }
    
    public int frameScore() {
        return frameScore;
    }

    public void setState(FrameState nextState) {
        state = nextState;
    }
    
}
