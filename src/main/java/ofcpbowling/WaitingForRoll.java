package ofcpbowling;


public class WaitingForRoll implements FrameState {

    private final BonusTransition spareTransition;
    
    public WaitingForRoll(BonusTransition spareTransition) {
        this.spareTransition = spareTransition;
    }

    public void roll(int score, Frame frame) {
        frame.addScoreToFrame(score);
        spareTransition.transition(frame);
    }

}
