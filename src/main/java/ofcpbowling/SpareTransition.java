package ofcpbowling;


public class SpareTransition {

    void transition(Frame frame) {
        if (frame.frameScore() == 10) {
            frame.setState(new WaitingForBonusRoll());
        } else {
            frame.setState(new FrameComplete());
        }
    }

}
