package ofcpbowling;


public class BonusTransition {
    
    private final FrameState nextState;
    private final FrameState bonusState;

    public BonusTransition(FrameState bonusState, FrameState nextState) {
        this.bonusState = bonusState;
        this.nextState = nextState;
    }

    void transition(Frame frame) {
        if (frame.frameScore() == 10) {
            frame.setState(bonusState);
        } else {
            frame.setState(nextState);
        }
    }
    
}
