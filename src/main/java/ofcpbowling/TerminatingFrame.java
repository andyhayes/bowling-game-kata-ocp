package ofcpbowling;

final class TerminatingFrame implements Frame {

    public int totalScore() {
        return 0;
    }

    public void roll(int i) {
    }

    public void addScoreToFrame(int score) {
        // do nothing
    }

    public void setState(FrameState nextState) {
        throw new UnsupportedOperationException("state changes not allowed");
    }

    public void passScoreToNextFrame(int score) {
        throw new UnsupportedOperationException("passing score to next frame not allowed");
    }

    public int frameScore() {
        return 0;
    }
}