package ofcpbowling;


public class BowlingGameFactory {

    public BowlingGame create() {
        Frame frame = new TerminatingFrame();
        for (int i=0; i<10; i++) {
            frame = createStandardFrame(frame);
        }
        return new BowlingGame(frame);
    }

    private Frame createStandardFrame(Frame frame) {
        BonusTransition spareTransition = new BonusTransition(new WaitingForBonusRoll(), new FrameComplete());
        WaitingForRoll waitingForOneRoll = new WaitingForRoll(spareTransition);
        BonusTransition strikeTransition = new BonusTransition(new WaitingForTwoBonusRolls(), waitingForOneRoll);
        WaitingForRoll waitingForTwoRolls = new WaitingForRoll(strikeTransition);
        return new StandardFrame(frame, waitingForTwoRolls);
    }

}
