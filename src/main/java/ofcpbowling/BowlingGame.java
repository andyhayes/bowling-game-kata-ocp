package ofcpbowling;

final class BowlingGame {

    private final Frame firstFrame;

    public BowlingGame(Frame firstFrame) {
        this.firstFrame = firstFrame;
    }

    public int score() {
        return firstFrame.totalScore();
    }

    public void roll(int score) {
        firstFrame.roll(score);
    }
}