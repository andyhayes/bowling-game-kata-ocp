package ofcpbowling;


public class FrameComplete implements FrameState {

    public void roll(int score, Frame frame) {
        frame.passScoreToNextFrame(score);
    }

}
