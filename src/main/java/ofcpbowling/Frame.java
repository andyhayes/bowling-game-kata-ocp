package ofcpbowling;


public interface Frame {

    public void roll(int score);

    public int totalScore();

    public void addScoreToFrame(int score);
    
    public void passScoreToNextFrame(int score);

    public void setState(FrameState nextState);

    public int frameScore();

}
