package ofcpbowling;


public class WaitingForTwoBonusRolls implements FrameState {

    public void roll(int score, Frame frame) {
        frame.addScoreToFrame(score);
        frame.passScoreToNextFrame(score);
        frame.setState(new WaitingForBonusRoll());
    }

}
